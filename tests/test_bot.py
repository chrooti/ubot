from asyncio import get_event_loop
from json import loads as json_loads
from os import environ
from os.path import abspath, basename, dirname
from unittest import TestCase

from telegen_definitions import InputFile, sendMessage, sendPhoto

from ubot import Bot


token = environ.get("TELEGRAM_TOKEN")
chat_id = environ.get("TELEGRAM_CHAT_ID")


def InputFile_from_path(path):
    with open(path, "rb") as f:
        content = f.read()
    return InputFile(basename(path), body=content)


class CustomBot(Bot):
    __slots__ = ["processed_updates"]

    async def after_handle(self, update):
        if not getattr(self, "processed_updates", None):
            self.processed_updates = 1
            return

        self.processed_updates += 1
        if self.processed_updates == 2:
            self.stop()


class BotTest(TestCase):
    def test_bot(self):
        bot = CustomBot(token)

        bot.push_update({"update_id": 0})
        bot.push_update({"update_id": 1})

        @bot.trigger
        async def test_sendMessage(bot, update):
            if update["update_id"] != 0:
                return

            req = sendMessage(chat_id, "Test")
            await bot.api_request(req)

        @bot.trigger
        async def test_sendPhoto(bot, update):
            if update["update_id"] != 1:
                return

            # test photo as file
            current_dir = dirname(abspath(__file__))
            photo = InputFile_from_path(f"{current_dir}/photo.jpg")

            req = sendPhoto(chat_id, photo)
            res = await bot.api_request(req)

            # test the same photo as file_id
            res = json_loads(res)
            photo.update(res["result"]["photo"][-1]["file_id"])

            req = sendPhoto(chat_id, photo)
            await bot.api_request(req)

        loop = get_event_loop()
        loop.run_until_complete(bot.start())
