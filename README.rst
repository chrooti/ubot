Ubot
####

Ubot is an implementation of a trigger-response loop for Telegram bots. Can (should) be extended depending on needs.

Docs
==========
https://strychnide.gitlab.io/ubot