API Reference
=============

.. autoclass:: ubot.Bot

    .. automethod:: ubot.Bot.before_handle
    .. automethod:: ubot.Bot.after_handle
    .. automethod:: ubot.Bot.get_type_and_flavor
    .. automethod:: ubot.Bot.api_request
    .. automethod:: ubot.Bot.start
    .. automethod:: ubot.Bot.stop
    .. automethod:: ubot.Bot.push_update
    .. automethod:: ubot.Bot.start_async
    .. autodecorator:: ubot.Bot.trigger
