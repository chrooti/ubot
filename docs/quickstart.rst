Quickstart
==========

(WIP section)

Simple echo bot
^^^^^^^^^^^^^^^

.. code-block:: python

    from ubot import Bot

    class EchoBot(Bot):
        async def before_handle(self, update):
            # check that we can actually echo the message
            self.get_type_and_flavor(update)
            return update["_flavor"] == "message" and "text" in update["message"]

    bot = EchoBot("token")

    @bot.trigger
    async def echo_trigger(bot, update):
        text = update["message"]["text"]
        chat_id = update["chat"]["id"]

        print(f"echoing message: {text} to chat: {chat_id}")

        await bot.api_request(("GET", "sendMessage", None, {
            "text": text, "chat_id": chat_id
        }, None))


Using telegram-definitions
^^^^^^^^^^^^^^^^^^^^^^^^^^
``WIP``


Webhook
^^^^^^^
``WIP``



