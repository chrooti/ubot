Welcome to ubot's documentation!
================================

Ubot is an implementation of a trigger-response loop for Telegram bots. Can (should) be extended depending on needs.


.. toctree::
    :maxdepth: 2
    :caption: Table of Contents

    quickstart
    api_reference
